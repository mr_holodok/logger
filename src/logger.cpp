#include <chrono>
#include <fcntl.h>
#include <iostream>
#include "logger.hpp"

Logger::Logger(FileStreamWrapper &fs, const std::string &fileName, NamedMutex &mutex)
 : _logFile {fs}, _mutex {mutex},
   _cMutexName {_cMutexNamePrefix + std::to_string(std::hash<std::string>()(fileName))}
{
    _logFile.open(fileName, std::ios::out | std::ios::app);
    if (!_logFile.is_open()) {
        throw std::runtime_error("Failed to open logging file!");
    }

    _mutex.open(_cMutexName);
}

Logger::~Logger()
{
    _logFile.close();
    _mutex.close();
}

void Logger::logInfo(const std::string& msg)
{
    log(msg, " [INFO] ");
}

void Logger::logError(const std::string& msg)
{
    log(msg, " [ERROR] ");
}

void Logger::log(const std::string &msg, const std::string &descriptor)
{
    // RAII wrapper for unlock in case of exceptions
    struct Unlocker {
        Unlocker(NamedMutex &mutex) : _mutex{mutex} {};
        ~Unlocker() { _mutex.unlock(); }

    private:
        NamedMutex &_mutex;
    } unlocker(_mutex);

    std::string timeStamp = getTimeStamp();

    _mutex.lock();

    _logFile.write(timeStamp);
    _logFile.write(descriptor);
    _logFile.write(msg);
    _logFile.writeEndLineAndFlush();

    _mutex.unlock();
}

std::string Logger::getTimeStamp()
{
    auto timePoint = std::chrono::system_clock::now();
    auto coarse = std::chrono::system_clock::to_time_t(timePoint);
    auto fine = std::chrono::time_point_cast<std::chrono::milliseconds>(timePoint);

    const size_t cBuffSize = sizeof "9999-12-31 23:59:59.999";
    char buffer[cBuffSize];

    const size_t cMsLength = 3;
    const long cMsValue = fine.time_since_epoch().count() % 1000;

    const char cDateTimeFormatStr[] = "%F %T."; // -> %Y-%m-%d %H:%M:%S.
    const char cMsFormatStr[] = "%03lu";

    // writing date and time
    const size_t cWrittenNumberOfChars = std::strftime(buffer, cBuffSize - cMsLength, cDateTimeFormatStr, std::localtime(&coarse));
    // appending ms to the end of buff
    std::snprintf(buffer + cWrittenNumberOfChars, cMsLength + 1, cMsFormatStr, cMsValue);

    return {buffer};
}
