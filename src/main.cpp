#include <iostream>
#include <cstring>
#include "logger.hpp"

namespace
{
    const std::string kUsageStr = "Usage: ./logger -n `file_name` -i `info msg` -e `error msg` -c `count`\n"
                                  "-n - log file name\n"
                                  "-i - write info message to log file\n"
                                  "-e - write error message to log file\n"
                                  "-c - write COUNT data to log file (has no sense when -i or -e are not present)\n";
    const std::string kHelpFlagLong = "--help";
    const std::string kHelpFlagShort = "-h";
    const std::string kFileNameFlag = "-n";
    const std::string kWriteInfoFlag = "-i";
    const std::string kWriteErrorFlag = "-e";
    const std::string kContinuousFlag = "-c";
}

int main(int argc, char **argv)
{
    if (argc == 1) {
        std::cout << kUsageStr << std::endl;
        return 0;
    }

    const int startIndex = 1;
    for (int argIndex = startIndex; argIndex < argc; ++argIndex) {
        if (std::strcmp(argv[argIndex], kHelpFlagLong.c_str()) == 0 ||
                std::strcmp(argv[argIndex], kHelpFlagLong.c_str()) == 0) {
            std::cout << kUsageStr << std::endl;
            return 0;
        }
    }

    std::string infoStr, errStr, logFileName;
    bool continuousWrite = false, needWriteInfo = false, needWriteError = false;
    int count = 1;

    for (int argIndex = startIndex; argIndex < argc; ++argIndex) {
        if (std::strcmp(argv[argIndex], kFileNameFlag.c_str()) == 0 &&
            argIndex + 1 < argc) {
            logFileName = std::string(argv[argIndex + 1]);
            ++argIndex;
        }
        else if (std::strcmp(argv[argIndex], kWriteInfoFlag.c_str()) == 0 &&
                argIndex + 1 < argc) {
            needWriteInfo = true;
            infoStr = std::string(argv[argIndex + 1]);
            ++argIndex;
        }
        else if (std::strcmp(argv[argIndex], kWriteErrorFlag.c_str()) == 0 &&
                 argIndex + 1 < argc) {
            needWriteError = true;
            errStr = std::string(argv[argIndex + 1]);
            ++argIndex;
        }
        else if (std::strcmp(argv[argIndex], kContinuousFlag.c_str()) == 0 &&
                 argIndex + 1 < argc) {
            continuousWrite = true;
            count = std::atoi(argv[argIndex + 1]);
            ++argIndex;
        }
    }

    if (logFileName.empty() || (infoStr.empty() && errStr.empty() && continuousWrite)) {
        std::cout << kUsageStr << std::endl;
        return 0;
    }

    try
    {
        FileStreamWrapper fsWrapper;
        SemaphoreInterface semaphoreInterface;
        NamedMutex mutex(&semaphoreInterface);
        Logger logger(fsWrapper, logFileName, mutex);

        do {
            if (needWriteInfo) {
                logger.logInfo(infoStr);
            }
            if (needWriteError) {
                logger.logError(errStr);
            }
            --count;
        } while (count > 0);
    }
    catch (const std::runtime_error &err)
    {
        std::cerr << "Caught error with message: " << err.what() << std::endl;
        return 1;
    }

    return 0;
}
