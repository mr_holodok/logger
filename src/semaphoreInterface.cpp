#include "semaphoreInterface.hpp"

sem_t *SemaphoreInterface::open(const char *name, int oflag, mode_t mode, int value) {
    return sem_open(name, oflag, mode, value);
}

sem_t *SemaphoreInterface::open(const char *name, int oflag) {
    return sem_open(name, oflag);
}

int SemaphoreInterface::wait(sem_t *semaphore) {
    return sem_wait(semaphore);
}

int SemaphoreInterface::post(sem_t *semaphore) {
    return sem_post(semaphore);
}

int SemaphoreInterface::close(sem_t *semaphore) {
    return sem_close(semaphore);
}

int SemaphoreInterface::unlink(const char *name) {
    return sem_unlink(name);
}


