//
// Created by mmoroz on 08.09.21.
//

#ifndef LOGGER_FILESTREAMWRAPPER_HPP
#define LOGGER_FILESTREAMWRAPPER_HPP

#include <fstream>

class FileStreamWrapper {
public:
    // TODO: add / delete other ctors ?
    FileStreamWrapper() = default;
    virtual ~FileStreamWrapper() = default;
    FileStreamWrapper(const FileStreamWrapper &other) = delete;

    virtual void open(const std::string &s, std::ios_base::openmode mode);
    virtual bool is_open();
    virtual void close();
    virtual void write(const std::string &s);
    virtual void writeEndLineAndFlush();

private:
    std::ofstream _file;
};

#endif //LOGGER_FILESTREAMWRAPPER_HPP
