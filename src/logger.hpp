#ifndef LOGGER_LOGGER_HPP
#define LOGGER_LOGGER_HPP

#include "fileStreamWrapper.hpp"
#include "namedMutex.hpp"
#include <string>

/*
 * Inter Process and Inter Thread synchronized logger
 *
 * Synchronization logic is based on named semaphores (POSIX).
 * Logger writes to file. If target file is not found then it is created.
 * Logger supports 2 types of messages: [INFO] and [ERROR]. Every log entry also has
 * time point of the record in format: yyyy-mm-dd HH:MM:SS.sss where sss - milliseconds
 * For error handling logger uses runtime_error exceptions.
 */

class Logger {
public:
    Logger(FileStreamWrapper &fs, const std::string &fileName, NamedMutex &mutex);
    ~Logger();

    void logInfo(const std::string& msg);
    void logError(const std::string& msg);

private:
    void log(const std::string &msg, const std::string &descriptor);
    static std::string getTimeStamp();

private:
    NamedMutex &_mutex;
    FileStreamWrapper &_logFile;

    const std::string _cMutexNamePrefix {"/global_mutex_"};
    // _cMutexName is combination of prefix and fileName hash
    // Why to use hash?
    // * for every log file named mutex (semaphore) should be unique, and the easiest way is to use file name
    //   to name mutex, or use file name indirectly (like hash);
    // * mutex file name shouldn't be more than NAME_MAX - 4 (i.e., 251) characters long
    //   so instead of creating complicated check + substring logic we can use hash functionality
    const std::string _cMutexName;
};

#endif //LOGGER_LOGGER_HPP
