#include <iostream>
#include <fcntl.h>
#include "namedMutex.hpp"

NamedMutex::~NamedMutex()
{
    closeImpl();
}

void NamedMutex::open(const std::string &name)
{
    // avoiding non-closed semaphores as result of multiple `open` calls
    if (_semaphore) {
        close();
    }

    _semaphoreName = name;
    const mode_t cSemPermissions = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH; // equal to 0644 -> rw-r--r--
    const int cSemValue = 1; // for exclusive access - mutex
    // trying to create new named semaphore
    _semaphore = _semaphoreLib->open(_semaphoreName.c_str(), O_CREAT | O_EXCL, cSemPermissions, cSemValue);
    if (_semaphore == SEM_FAILED) {
        // such semaphore already exists, so just open it
        if (errno == EEXIST) {
            _semaphore = _semaphoreLib->open(_semaphoreName.c_str(), 0);
        }
        if (_semaphore == SEM_FAILED) {
            throw std::runtime_error("Failed to open semaphore with call to sem_open!");
        }
    } else {
        // semaphore was created (not just opened) so current object should unlink the semaphore
        _shouldUnlink = true;
    }
}

void NamedMutex::close()
{
    closeImpl();
}

void NamedMutex::unlink()
{
    if (_semaphoreLib->unlink (_semaphoreName.c_str()) == -1) {
        std::cerr << "sem_unlink failed" << std::endl;
    }
}

void NamedMutex::lock()
{
    if (!_semaphore || _semaphoreLib->wait(_semaphore) == -1) {
        throw std::runtime_error("Call to sem_wait failed!");
    }
}

void NamedMutex::unlock()
{
    if (!_semaphore || _semaphoreLib->post(_semaphore) == -1) {
        throw std::runtime_error("Call to sem_post failed!");
    }
}

void NamedMutex::closeImpl()
{
    // call to `open` can fail so there is nothing to clean up
    if (!_semaphore) {
        return;
    }

    if (_shouldUnlink) {
        unlink();
    }

    if (_semaphoreLib->close(_semaphore) == -1) {
        std::cerr << "sem_close failed" << std::endl;
    }

    // for multiple open/close actions
    _semaphore = nullptr;
}

NamedMutex::NamedMutex(SemaphoreInterface *s)
  : _semaphoreLib {s}
{}




