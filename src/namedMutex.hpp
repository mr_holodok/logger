#ifndef LOGGER_NAMEDMUTEX_HPP
#define LOGGER_NAMEDMUTEX_HPP

#include "semaphoreInterface.hpp"
#include <string>

class NamedMutex {
public:
    NamedMutex() = delete;
    explicit NamedMutex(SemaphoreInterface *s);
    virtual ~NamedMutex();
    NamedMutex(const NamedMutex &other) = delete;

    virtual void open(const std::string &name);
    virtual void close();

    virtual void lock();
    virtual void unlock();

protected:
    // `unlink` is virtual for testing purposes
    virtual void unlink();
    // prevent calling virtual `close` in destructor
    void closeImpl();

private:
    SemaphoreInterface *_semaphoreLib;
    sem_t *_semaphore {nullptr};
    std::string _semaphoreName;
    bool _shouldUnlink {false};
};

#endif //LOGGER_NAMEDMUTEX_HPP
