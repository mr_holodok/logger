//
// Created by mmoroz on 08.09.21.
//

#include "fileStreamWrapper.hpp"

void FileStreamWrapper::open(const std::string &s, std::ios_base::openmode mode)
{
    _file.open(s, mode);
}

bool FileStreamWrapper::is_open()
{
    return _file.is_open();
}

void FileStreamWrapper::close()
{
    _file.close();
}

void FileStreamWrapper::write(const std::string &s)
{
    _file << s;
}

void FileStreamWrapper::writeEndLineAndFlush()
{
    _file << std::endl;
}


