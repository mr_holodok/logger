#ifndef LOGGER_SEMAPHOREINTERFACE_HPP
#define LOGGER_SEMAPHOREINTERFACE_HPP

#include <semaphore.h>

class SemaphoreInterface {
public:
    SemaphoreInterface() = default;
    virtual ~SemaphoreInterface() = default;

    virtual sem_t* open(const char *name, int oflag, mode_t mode, int value);
    virtual sem_t* open(const char *name, int oflag);
    virtual int wait(sem_t *semaphore);
    virtual int post(sem_t *semaphore);
    virtual int close(sem_t *semaphore);
    virtual int unlink(const char *name);
};

#endif //LOGGER_SEMAPHOREINTERFACE_HPP
