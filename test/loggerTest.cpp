#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "mockFileStreamWrapper.hpp"
#include "mockNamedMutex.hpp"
#include "mockSemaphoreInterface.hpp"
#include "../src/logger.hpp"

class LoggerTest : public ::testing::Test {
protected:
    MockFileStreamWrapper _fstream;
    MockSemaphoreInterface _semInterface;
    MockNamedMutex _mutex {&_semInterface};
    const std::string _cFileName {"test_log.txt"};
};

TEST_F(LoggerTest, OpensLogFile)
{
    EXPECT_CALL(_fstream, open(_cFileName, std::ios::out | std::ios::app))
        .Times(1);
    EXPECT_CALL(_fstream, is_open())
        .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
}

TEST_F(LoggerTest, OpensLogFileWithException)
{
    EXPECT_CALL(_fstream, open(::testing::_, ::testing::_))
            .WillOnce([]() { throw std::ios_base::failure("Some internal error!"); });

    const std::string cFileNameWithError {"test_log_error.txt"};
    ASSERT_THROW(Logger logger(_fstream, cFileNameWithError, _mutex), std::runtime_error);
}

TEST_F(LoggerTest, OpensLogFileWithError)
{
    // check case when file is not opened/created (ex. access rights needed, name is empty, etc)
    EXPECT_CALL(_fstream, is_open())
            .WillOnce(::testing::Return(false));

    const std::string cFileNameWithError {"test_log_error.txt"};
    ASSERT_THROW(Logger logger(_fstream, cFileNameWithError, _mutex), std::runtime_error);
}

TEST_F(LoggerTest, WritesInfoToLogFile)
{
    EXPECT_CALL(_fstream, write(::testing::_))
            .Times(::testing::AtLeast(1));
    EXPECT_CALL(_fstream, writeEndLineAndFlush())
            .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
    logger.logInfo("some info");
}

TEST_F(LoggerTest, ChecksInfoInLogFile)
{
    const std::string cInfoString {"some info"};
    Logger logger(_fstream, _cFileName, _mutex);
    logger.logInfo(cInfoString);

    ASSERT_TRUE(_fstream.checkInContent(cInfoString));
}

TEST_F(LoggerTest, WritesErrorToLogFile)
{
    EXPECT_CALL(_fstream, write(::testing::_))
            .Times(::testing::AtLeast(1));
    EXPECT_CALL(_fstream, writeEndLineAndFlush())
            .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
    logger.logError("some info");
}

TEST_F(LoggerTest, ChecksErrorInLogFile)
{
    const std::string cErrorString {"some info"};
    Logger logger(_fstream, _cFileName, _mutex);
    logger.logInfo(cErrorString);

    ASSERT_TRUE(_fstream.checkInContent(cErrorString));
}

TEST_F(LoggerTest, ClosesLogFile)
{
    EXPECT_CALL(_fstream, close())
            .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
}

TEST_F(LoggerTest, OpensMutex)
{
    EXPECT_CALL(_mutex, open(::testing::_))
            .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
}

TEST_F(LoggerTest, OpensMutexWithException)
{
    using ::testing::_;

    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .WillOnce(::testing::Return(SEM_FAILED));

    ASSERT_THROW(Logger logger(_fstream, _cFileName, _mutex), std::runtime_error);
}

TEST_F(LoggerTest, MutexLockUnlock)
{
    EXPECT_CALL(_mutex, lock())
            .Times(1);
    EXPECT_CALL(_mutex, unlock())
            .Times(2);

    Logger logger(_fstream, _cFileName, _mutex);
    logger.logInfo("some info");
}

TEST_F(LoggerTest, MutexLockUnlockEveryTime)
{
    testing::InSequence seq;

    EXPECT_CALL(_mutex, lock())
            .Times(1);
    EXPECT_CALL(_mutex, unlock())
            .Times(2);
    EXPECT_CALL(_mutex, lock())
            .Times(1);
    EXPECT_CALL(_mutex, unlock())
            .Times(2);

    Logger logger(_fstream, _cFileName, _mutex);
    logger.logInfo("some info 1");
    logger.logInfo("some info 2");
}

TEST_F(LoggerTest, ClosesMutex)
{
    EXPECT_CALL(_mutex, close())
            .Times(1);

    Logger logger(_fstream, _cFileName, _mutex);
}

TEST_F(LoggerTest, FailsWhenLocking)
{
    EXPECT_CALL(_semInterface, wait(::testing::_))
            .WillOnce(::testing::Return(-1))
            .WillOnce(::testing::Return(-1));

    Logger logger(_fstream, _cFileName, _mutex);
    ASSERT_THROW(logger.logInfo("some info"), std::runtime_error);
    ASSERT_THROW(logger.logError("some info"), std::runtime_error);
}

TEST_F(LoggerTest, FailsWhenUnlockingToWriteInfo)
{
    EXPECT_CALL(_semInterface, post(::testing::_))
            .Times(::testing::AtLeast(1))
            .WillOnce([this] (sem_t* semaphore) { _semInterface.post(semaphore); return -1; });

    Logger logger(_fstream, _cFileName, _mutex);

    ASSERT_THROW(logger.logInfo("some info"), std::runtime_error);
}

TEST_F(LoggerTest, FailsWhenWritingToFile)
{
    EXPECT_CALL(_fstream, write(::testing::_))
            .WillOnce([]() { throw std::fstream::failure("Error while writing!"); })
            .WillOnce([](){})
            .WillOnce([]() { throw std::fstream::failure("Error while writing!"); })
            .WillOnce([](){})
            .WillOnce([](){})
            .WillOnce([]() { throw std::fstream::failure("Error while writing!"); })
            .WillRepeatedly([](){});
    EXPECT_CALL(_fstream, writeEndLineAndFlush())
            .WillOnce([]() { throw std::fstream::failure("Error while writing!"); });

    Logger logger(_fstream, _cFileName, _mutex);
    EXPECT_THROW(logger.logInfo("some info"), std::runtime_error);
    EXPECT_THROW(logger.logInfo("some info"), std::runtime_error);
    EXPECT_THROW(logger.logInfo("some info"), std::runtime_error);
    EXPECT_THROW(logger.logInfo("some info"), std::runtime_error);
}