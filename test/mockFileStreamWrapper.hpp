#ifndef LOGGER_MOCKFILESTREAMWRAPPER_HPP
#define LOGGER_MOCKFILESTREAMWRAPPER_HPP

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "../src/fileStreamWrapper.hpp"

class MockFileStreamWrapper : public FileStreamWrapper {
public:
    MockFileStreamWrapper()
    {
        ON_CALL(*this, is_open).WillByDefault(::testing::Return(true));
        ON_CALL(*this, write).WillByDefault([this](const std::string &s) { _fileContent << s; } );
        ON_CALL(*this, writeEndLineAndFlush()).WillByDefault([this]() { _fileContent << std::endl; });
    }

    MOCK_METHOD(void, open, (const std::string &s, std::ios_base::openmode mode), (override));
    MOCK_METHOD(bool, is_open, (), (override));
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(void, write, (const std::string &s), (override));
    MOCK_METHOD(void, writeEndLineAndFlush, (), (override));

    bool checkInContent(const std::string& data)
    {
        const std::string cContent = _fileContent.str();
        return cContent.find(data) != std::string::npos;
    }

private:
    std::stringstream _fileContent;
};

#endif //LOGGER_MOCKFILESTREAMWRAPPER_HPP
