#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "mockNamedMutex.hpp"
#include "mockSemaphoreInterface.hpp"

using ::testing::_;

class NamedMutexTest : public ::testing::Test {
protected:
    MockSemaphoreInterface _semInterface;
    NamedMutex _mutex {&_semInterface};
    const std::string _cMutexName {"/global_mutex"};
};

TEST_F(NamedMutexTest, SuccessfullyCreatesMutex)
{
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .Times(1);

    ASSERT_NO_THROW(_mutex.open(_cMutexName));
}

TEST_F(NamedMutexTest, SuccessfullyOpensMutex)
{
    NamedMutex mutex2 {&_semInterface};

    ::testing::InSequence inSeq;
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .Times(1);
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .WillOnce([](const char *name, int oflag, mode_t mode, int value) { errno = EEXIST; return SEM_FAILED; });
    EXPECT_CALL(_semInterface, open(_, _))
            .Times(1);

    EXPECT_NO_THROW(_mutex.open(_cMutexName));
    EXPECT_NO_THROW(mutex2.open(_cMutexName));

    // reset global error state
    errno = EINTR;
}

TEST_F(NamedMutexTest, ClosesEveryOpenedMutex)
{
    const std::string cNewMutexName {"/global_new_mutex"};
    EXPECT_CALL(_semInterface, close(_))
            .Times(2);

    EXPECT_NO_THROW(_mutex.open(_cMutexName));
    EXPECT_NO_THROW(_mutex.open(cNewMutexName));
    EXPECT_NO_THROW(_mutex.close());
}

TEST_F(NamedMutexTest, FailsWhenOpensMutex)
{
    const std::string cIllegalMutexName {"/"};
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .WillOnce(::testing::Return(SEM_FAILED));
    EXPECT_CALL(_semInterface, open(_, _))
            .Times(0);

    ASSERT_THROW(_mutex.open(cIllegalMutexName), std::runtime_error);
}

TEST_F(NamedMutexTest, FailsWhenLocksMutexWithoutOpen)
{
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .Times(0);
    EXPECT_CALL(_semInterface, open(_, _))
            .Times(0);
    EXPECT_CALL(_semInterface, wait(_))
            .Times(0);

    ASSERT_THROW(_mutex.lock(), std::runtime_error);
}

TEST_F(NamedMutexTest, FailsFromExternalErrorWhenLocksMutex)
{
    EXPECT_CALL(_semInterface, wait(_))
            .WillOnce(::testing::Return(-1));

    _mutex.open(_cMutexName);

    ASSERT_THROW(_mutex.lock(), std::runtime_error);
}

TEST_F(NamedMutexTest, FailsWhenUnlocksMutexWithoutOpen)
{
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .Times(0);
    EXPECT_CALL(_semInterface, open(_, _))
            .Times(0);
    EXPECT_CALL(_semInterface, wait(_))
            .Times(0);
    EXPECT_CALL(_semInterface, post(_))
            .Times(0);

    ASSERT_THROW(_mutex.unlock(), std::runtime_error);
}

TEST_F(NamedMutexTest, FailsFromExternalErrorWhenUnlocksMutex)
{
    EXPECT_CALL(_semInterface, post(::testing::_))
            .WillOnce(::testing::Return(-1));

    _mutex.open(_cMutexName);
    _mutex.lock();

    ASSERT_THROW(_mutex.unlock(), std::runtime_error);
}

TEST_F(NamedMutexTest, NotFailsWhenClosesMutexWithoutOpen)
{
    EXPECT_CALL(_semInterface, open(_, _, _, _))
            .Times(0);
    EXPECT_CALL(_semInterface, open(_, _))
            .Times(0);
    EXPECT_CALL(_semInterface, close(_))
            .Times(0);

    ASSERT_NO_THROW(_mutex.close());
}

TEST_F(NamedMutexTest, NotFailsWhenClosesMutexWithCloseError)
{
    ::testing::InSequence inSeq;
    EXPECT_CALL(_semInterface, unlink(_))
            .Times(1);
    EXPECT_CALL(_semInterface, close(_))
            .WillOnce(::testing::Return(-1));

    _mutex.open(_cMutexName);

    ASSERT_NO_THROW(_mutex.close());
}

TEST_F(NamedMutexTest, CallsUnlinkOnceWhenSingleMutex)
{
    ::testing::InSequence inSeq;
    EXPECT_CALL(_semInterface, unlink(_))
            .Times(1);
    EXPECT_CALL(_semInterface, close(_))
            .Times(1);

    _mutex.open(_cMutexName);

    ASSERT_NO_THROW(_mutex.close());
}

TEST_F(NamedMutexTest, CallsUnlinkOnceWhenMultipleMutexes)
{
    MockSemaphoreInterface semInterface2;

    ::testing::InSequence inSeq;
    EXPECT_CALL(semInterface2, open(_, _, _, _))
            .WillOnce([](const char *name, int oflag, mode_t mode, int value) { errno = EEXIST; return SEM_FAILED; });
    EXPECT_CALL(semInterface2, open(_, _))
            .Times(1);

    NamedMutex mutex2 {&semInterface2};

    EXPECT_CALL(_semInterface, unlink(::testing::_))
            .Times(1);
    EXPECT_CALL(semInterface2, unlink(::testing::_))
            .Times(0);

    _mutex.open(_cMutexName);
    mutex2.open(_cMutexName);

    EXPECT_NO_THROW(_mutex.close());
    EXPECT_NO_THROW(mutex2.close());

    // reset global error state
    errno = EINTR;
}

TEST_F(NamedMutexTest, NotFailsWhenUnlinksWithError)
{
    EXPECT_CALL(_semInterface, unlink(_))
            .WillOnce([] (const char *name) { return -1; });

    _mutex.open(_cMutexName);

    EXPECT_NO_THROW(_mutex.close());
}