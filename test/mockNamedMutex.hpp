#ifndef LOGGER_MOCKNAMEDMUTEX_HPP
#define LOGGER_MOCKNAMEDMUTEX_HPP

#include "../src/namedMutex.hpp"
#include <gmock/gmock.h>

class MockNamedMutex : public NamedMutex {
public:
    MockNamedMutex(SemaphoreInterface *semaphoreInterface) : NamedMutex(semaphoreInterface)
    {
        ON_CALL(*this, open).WillByDefault([this](const std::string &name) {
            return this->NamedMutex::open(name);
        });
        ON_CALL(*this, lock).WillByDefault([this]() {
            return this->NamedMutex::lock();
        });
        ON_CALL(*this, unlock).WillByDefault([this]() {
            return this->NamedMutex::unlock();
        });
        ON_CALL(*this, close).WillByDefault([this]() {
            return this->NamedMutex::close();
        });
        ON_CALL(*this, unlink).WillByDefault([this]() {
            return this->NamedMutex::unlink();
        });
    }
    ~MockNamedMutex() override = default;

    MOCK_METHOD(void, open, (const std::string &name), (override));
    MOCK_METHOD(void, lock, (), (override));
    MOCK_METHOD(void, unlock, (), (override));
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(void, unlink, (), (override));
};

#endif //LOGGER_MOCKNAMEDMUTEX_HPP
