#ifndef LOGGER_MOCKSEMAPHOREINTERFACE_HPP
#define LOGGER_MOCKSEMAPHOREINTERFACE_HPP

#include "../src/semaphoreInterface.hpp"
#include <gmock/gmock.h>

class MockSemaphoreInterface : public SemaphoreInterface {
public:
    MockSemaphoreInterface()
    {
        using ::testing::_;
        ON_CALL(*this, open(_, _, _, _)).WillByDefault(::testing::Return(&dummy));
        ON_CALL(*this, open(_, _)).WillByDefault(::testing::Return(&dummy));
        ON_CALL(*this, wait).WillByDefault(::testing::Return(0));
        ON_CALL(*this, post).WillByDefault(::testing::Return(0));
        ON_CALL(*this, close).WillByDefault(::testing::Return(0));
        ON_CALL(*this, unlink).WillByDefault(::testing::Return(0));
    }
    ~MockSemaphoreInterface() override = default;

    using SemaphoreInterface::open;
    MOCK_METHOD(sem_t*, open, (const char *name, int oflag, mode_t mode, int value), (override));
    MOCK_METHOD(sem_t*, open, (const char *name, int oflag), (override));
    MOCK_METHOD(int, wait, (sem_t *semaphore), (override));
    MOCK_METHOD(int, post, (sem_t *semaphore), (override));
    MOCK_METHOD(int, close, (sem_t *semaphore), (override));
    MOCK_METHOD(int, unlink, (const char *name), (override));

private:
    sem_t dummy;
};

#endif //LOGGER_MOCKSEMAPHOREINTERFACE_HPP
