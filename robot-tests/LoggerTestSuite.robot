*** Settings ***
Suite Setup       Remove File    ${logFileName}
Suite Teardown    Remove File    ${logFileName}
Library           OperatingSystem
Library           Process

*** Variables ***
${logFileName}    testlog.txt
${loggerExeName}    logger
${timestampRegex}    \\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]) ([01]\\d|2[0-3]):([0-5]\\d):([0-5]\\d).\\d{3}
${infoLabel}      [INFO]
${errorLabel}     [ERROR]
${infoDemoText}    some demo info text
${errorDemoText}    some demo error text

*** Test Cases ***
CreateNewLogFile
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName}
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    File Should Be Empty    ${logFileName}    Created file not empty

OpenFileWithNoChange
    [Setup]    Create File    ${logFileName}    ${infoDemoText}
    ${logContentBefore} =    Run    cat ${logFileName}
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName}
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    ${logContentAfter} =    Run    cat ${logFileName}
    Should Be Equal As Strings    ${logContentBefore}    ${logContentAfter}    Content of log file is changed!

WriteInfoToLog
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -i "${infoDemoText}"
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    File Should Not Be Empty    ${logFileName}    Log file is empty
    ${logContent} =    Run    cat ${logFileName}
    Should Match Regexp    ${logContent}    ${timestampRegex} \\[INFO\\] ${infoDemoText}

WriteErrorToLog
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -e "${errorDemoText}"
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    File Should Not Be Empty    ${logFileName}    Log file is empty
    ${logContent} =    Run    cat ${logFileName}
    Should Match Regexp    ${logContent}    ${timestampRegex} \\[ERROR\\] ${errorDemoText}

WriteInfoAndErrorToLog
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -i "${infoDemoText}" -e "${errorDemoText}"
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    File Should Not Be Empty    ${logFileName}    Log file is empty
    ${logContent} =    Run    cat ${logFileName}
    Should Match Regexp    ${logContent}    ${timestampRegex} \\[INFO\\] ${infoDemoText}\n${timestampRegex} \\[ERROR\\] ${errorDemoText}

AppendInfoToLog
    [Setup]    Create File    ${logFileName}    2021-09-15 10:00:00.000 [INFO] some previous info
    ${logContentBefore} =    Run    cat ${logFileName}
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -i "${infoDemoText}"
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    ${logContentAfter} =    Run    cat ${logFileName}
    Should Contain    ${logContentAfter}    ${logContentBefore}
    Should Contain    ${logContentAfter}    ${infoLabel} ${infoDemoText}

AppendErrorToLog
    [Setup]    Create File    ${logFileName}    2021-09-15 10:00:00.000 [INFO] some previous info
    ${logContentBefore} =    Run    cat ${logFileName}
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -e "${errorDemoText}"
    Should Be Equal As Integers    ${rc}    0    Return code is not 0
    ${logContentAfter} =    Run    cat ${logFileName}
    Should Contain    ${logContentAfter}    ${logContentBefore}
    Should Contain    ${logContentAfter}    ${errorLabel} ${errorDemoText}

ParallelWrite
    ${msgProc1} =    Set Variable    Proc1 message
    ${msgProc2} =    Set Variable    Proc2 message
    ${msgCount} =    Set Variable    5
    ${handleProc1} =    Start Process    ${CURDIR}${/}${loggerExeName}    -n    ${logFileName}    -i    "${msgProc1}"    -c    ${msgCount}
    ${handleProc2} =    Start Process    ${CURDIR}${/}${loggerExeName}    -n    ${logFileName}    -i    "${msgProc2}"    -c    ${msgCount}
    ${procResult1} =    Wait For Process    ${handleProc1}
    ${procResult2} =    Wait For Process    ${handleProc2}
    Should Be Equal As Integers    ${procResult1.rc}    0    Return code is not 0
    Should Be Equal As Integers    ${procResult2.rc}    0    Return code is not 0
    ${logContent} =    Run    cat ${logFileName}
    Should Contain X Times    ${logContent}    ${msgProc1}    ${msgCount}
    Should Contain X Times    ${logContent}    ${msgProc2}    ${msgCount}

FailWhenCanNotOpenFile
    [Setup]    Create Log Without Access
    ${rc} =    Run And Return Rc    ${CURDIR}${/}${loggerExeName} -n ${logFileName} -i "${infoDemoText}"
    Should Be Equal As Integers    ${rc}    1    Return code is not 1

*** Keywords ***
Create Log Without Access
    Run    touch ${logFileName}
    Run    chmod 400 ${logFileName}
